\documentclass[11pt]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{a4wide}

\usepackage{ragged2e}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{hyperref}
\usepackage{cleveref}

%% More subsections
\usepackage{titlesec}
\titleformat{\paragraph}[hang]{\normalfont\normalsize\bfseries}{\theparagraph}{1em}{}
\titlespacing*{\paragraph}{0pt}{3.25ex plus 1ex minus .2ex}{0.5em}

%% Tables
\usepackage{booktabs}
\renewcommand{\arraystretch}{1.2}

%% Fonts
\usepackage[bitstream-charter]{mathdesign}
\usepackage[scaled=.90]{berasans}
\usepackage[scaled]{beramono}

\usepackage{xcolor}
\definecolor{darkgreen}{rgb}{0.0, 0.3, 0.23}
\definecolor{x11gray}{rgb}{0.90, 0.90, 0.90}
\definecolor{blue}{rgb}{0.2, 0.2, 0.6}

%% Listings text
\usepackage{listings}
\lstset{language={},
       %backgroundcolor=\color{x11gray},
       frame=tb,
       basicstyle=\color{blue}\ttfamily\footnotesize,
       captionpos=b,
       tabsize=2,
  }

\linespread{1.2}

\newcommand{\VERSION}{v0.1}
\newcommand{\cmd}[1]{\\{\color{darkgreen}\small\texttt{\$ {#1}}}}

\title{\huge Science For Everyone (Scife) \\ Reference guide \VERSION}
\author{Raúl Moreno Galdón\\}

\date{\today}

\begin{document}

\maketitle

\newpage

\section{Installation}

In this section we explain the steps for performing a Scife installation on a single processing node.
It is recommended to follow the installation steps on a virtual machine to avoid unexpected compatibility problems, especially in the case of using a non tested operative system.

\subsection{Platform and software requirements}

\subsubsection{Processing node layout}

Scife can be installed on platforms with different layouts as each process has its own IP address and port.
Thus, the range of configuration is very wide as Scife can use multiple processing nodes for hosting its services.
The most simple installation consists on a single node deployment, in which all Scife services are installed on the same machine.
This way, most of the network and security steps are simplified as all services communicate on the same machine.
We will use the single node layout in this guide.

However, other more elaborate layouts could use an individual processing node for each service, using an interconnection network for performing the communication between the services.
This kind of layout can provide a more stable and fault-tolerant installation of Scife.

\subsubsection{Software dependencies}

Scife needs some software dependencies to work.
\Cref{tab:software} summarizes the software requirements of Scife.
There are no hardware requirements specified, as they depend on the load of the system (users, concurrent experiments, disk storage).
Also, no stress tests has been performed on our test systems.

\begin{table}[!h]
    \centering
    \begin{tabular}{l l}
        \toprule
        Software & Tested \\
        \midrule
        Linux Operative System & CentOS, Fedora \\
        Git \& Git daemon & Git v2.21 \\
        Rsync \& Rsync daemon & Rsync v3.1 \\
        MongoDB server & MongoDB Community Edition v4.0 \\
        NodeJS and packages & NodeJS v5.5 \\
        \bottomrule
    \end{tabular}
    \caption{Software dependencies of Scife.\label{tab:software}}
\end{table}

\subsection{Installation steps (using a virtual machine)}

A basic Scife installation is detailed in the next sections, using a virtual machine for hosting the software.

\subsubsection{Creating a Virtual Machine}

We will install Scife on a Virtual Machine (VM), this allow us to use any processing node with any operative system (OS) we have available.
The advantage of using a VM is that we can install any OS and software without disturbing, or being influenced, by the hosting system.
The steps for creating a VM varies a lot depending on your platform, so we cannot provide a series of steps valid for all platforms.

However, we can provide some suggestions.
In the case of using a Windows system, you could use \href{https://www.virtualbox.org}{Virtual Box} or \href{https://www.vmware.com}{VMware}.
In the case of using a Linux system, you could use \href{https://www.virtualbox.org}{Virtual Box} or \href{https://www.qemu.org}{QEMU}.
You can also host a VM on a Cloud provider.

Regarding the size of the VM, 1 virtual CPU with 1 GB of RAM should be enough for a test installation of Scife.
Also, you should install a CentOS (6 or superior) or Fedora (25 or superior) OS.
When installing the OS, create the user \texttt{user} (any other name is OK) and its \texttt{home} folder, e.g. \texttt{/home/user}.
This user must be allowed to use the \texttt{sudo} command.

\subsubsection{Installing dependencies}
\label{sec:dependencies}

In the following sections we assume that the user has access to a VM with a CentOS or Fedora OS.
Also, we are going to assume that the IP address of the VM is \texttt{192.168.1.10}.
When using CentOS or Fedora, installing Git, Rsync and their daemons is straightforward using the package manager, so just run the command:
\cmd{sudo yum install rsync git git-daemon}

The installation of MongoDB (Community Edition) requires an additional step, its repository must be included on the system.
For that, create the \texttt{/etc/yum.repos.d/mongodb-org-4.0.repo} file with the following contents:
\begin{lstlisting}
[mongodb-org-4.0]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/4.0/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-4.0.asc
\end{lstlisting}

After creating that file, just run the following command (and accept the keys if asked) to install MongoDB:
\cmd{sudo yum install mongodb-org}

Now, we will download Scife to our system.
For that we will use Git to clone the project from the repository, and saving it to the \texttt{/home/user/scife} folder:
\cmd{git clone https://github.com/raulmorenogaldon/Scife.git /home/user/scife}

We now proceed to install the NodeJS environment.
For that purpose, the Node Version Manager (\href{https://github.com/nvm-sh/nvm}{NVM}) is a helper software that allows us to install any version of NodeJS.
We provided the script \texttt{install\_nvm.sh} in the \textbf{extra} folder of Scife.
We can install NVM and NodeJS v5.5.0 by running this script from the root folder of Scife:
\cmd{cd /home/user/scife}
\cmd{./extra/install\_nvm.sh}
\cmd{bash}
\cmd{nvm install v5.5.0}

Now that we have installed NodeJS v5.5.0, we have to load it by running:
\cmd{nvm use v.5.5.0}

The last step is to install all the remaining Scife dependencies.
For that run:
\cmd{npm install}

After a while, the dependencies should be successfully downloaded and installed.

The final step would consist on generating a key-pair for authentication on the system via SSH.
Scife minions communicates with the target processing platform using an SSH connection.
Therefore, we will set an SSH key pair to avoid passing a password every time the minion send a command to the target processing platform:
\cmd{ssh-keygen -t rsa -b 4096}

Accept all the prompts.
Now, add the key to the authorized keys and perform the first connection:
\cmd{ssh-copy-id -i /home/user/.ssh/id\_rsa.pub user@localhost}
\cmd{ssh localhost}

Accept all the prompts.
From this moment, the \texttt{ssh localhost} command should work without asking anything to the user.

\subsubsection{Configuring Scife}

The next step is to configure Scife itself.
It is important to create a deployment plan, specifying which services will be deployed to which node and TCP ports.
In our case we are going to use a single node deployment, so we need to specify which ports to use for every service.
We propose a setup in \cref{tab:deployment}.

\begin{table}[!h]
    \centering
    \begin{tabular}{l l l c}
        \toprule
        Service & IP & Listen & Port (TCP) \\
        \midrule
        Overlord & 192.168.1.10 & 0.0.0.0 & 4000 \\
        Storage & 192.168.1.10 & 0.0.0.0 & 4001 \\
        Git & 192.168.1.10 & 0.0.0.0 & 4002 \\
        Rsync & 192.168.1.10 & 0.0.0.0 & 4003 \\
        MongoDB & 192.168.1.10 & 0.0.0.0 & 4004 \\
        Web & 192.168.1.10 & 0.0.0.0 & 4005 \\
        Minion & 192.168.1.10 & 0.0.0.0 & 4010 \\
        \bottomrule
    \end{tabular}
    \caption{Deployment configuration of Scife services.\label{tab:deployment}}
\end{table}

The \texttt{192.168.1.10} is the IP address of our virtual machine.
Note that the port of the \textit{web} service is not the same as the HTTPS port.
In this case the web service will communicate with the \textit{overlord} service using the port 4005, and provide the web interface on the HTTPS port (443).

In this configuration, the services are listening to petitions coming from any IP address.
To increase security, we could change the listening IPs to match the addresses of the other services (e.g. to 127.0.0.1).

Regarding the database, we are going to use a database named \texttt{db}, which will be created automatically after configuring Scife.
Once we have a deployment configuration, we can create the configuration files of our Scife system.

\paragraph{Overlord configuration file}

A template of the overlord configuration file can be found on the \textbf{extra} folder.
Using the information in \cref{tab:deployment}, we would create a \textbf{\texttt{/home/user/config/overlord.cfg}} file with the following contents:

\begin{lstlisting}
{
    "OVERLORD_LISTEN_PORT":"4000",             # Specify the port.
    "OVERLORD_IP":"192.168.1.10",              # IP of the overlord service
    "MONGO_URL":"mongodb://localhost:4004/db", # URL of MongoDB server.
    "MINION_URL":["tcp://localhost:4010"],     # List of minions
    "STORAGE_URL":"tcp://localhost:4001",      # URL of the storage service.
    "SECRET":"d6e86ff5-06de-***",              # Secret for encoding auth token.
    "AUTOUPDATE":"master"                      # (Experimental) Autoupdates.
}
\end{lstlisting}

Notice the brackets at the beginning and at the end of the file, these are necessary because the file is in JSON format.
For each service, we set their IP address and their corresponding port.
In this file we set the IP of the other services to \texttt{localhost}, which in our case is equivalent to set it to \texttt{192.168.1.10}, and may improve communications performance.
Note also that we specified the name of the database \texttt{db} in the \texttt{MONGO\_URL} field.

The string of the \texttt{SECRET} field is used by the system for encoding the authentication tokens and should be known only by this service.
Therefore, \textbf{do not use} the contents of the \texttt{SECRET} field showed in this document.
Instead, use the \texttt{gen\_uuid.sh} script for generating a new \texttt{SECRET} string:
\cmd{extra/gen\_uuid.sh}

The \texttt{MINION\_URL} is an array of URL, so you can specify multiple minions.
For example, we could set the field to: \texttt{["tcp://localhost:4010", "tcp://192.168.1.12:4010"]}.
In this example, there is an additional minion on another processing node, listening on the same port (4010).

The \texttt{AUTOUPDATE} field is an experimental feature for autoupdating Scife from its Git repository, using the branch name specified on this field.
This feature is currently unsupported and therefore it will not have effect on the system (you can even remove this field).

\paragraph{Storage configuration file}

A template of the storage configuration file can be found on the \textbf{extra} folder.
Using the information in \cref{tab:deployment}, we would create a \textbf{\texttt{/home/user/config/storage.cfg}} file with the following contents:

\begin{lstlisting}
{
    # Folder path where all the application data will be stored.
    "appstorage": "/home/user/appstorage",
    # Folder path where all the input files will be stored.
    "inputstorage": "/home/user/inputstorage",
    # Folder path where all the output files will be stored.
    "outputstorage": "/home/user/outputstorage",
    # Folder path where all Rsync config files will be generated.
    "rsync_folder": "/home/user/rsync_conn",
    # IP of the machine on which the storage service will be listening.
    "public_url": "192.168.1.10",
    # Port on which the Git daemon is listening (on the local machine).
    "git_port": "4002",
    # Port on which the Rsync daemon is listening (on the local machine).
    "rsync_port": "4003",
    # IPs and port on which the storage service will be listening.
    "listen": "tcp://0.0.0.0:4001",
    # URL of the MongoDB service and database name.
    "db": "mongodb://localhost:4004/db"
}
\end{lstlisting}

Notice the brackets at the beginning and at the end of the file, these are necessary because the file is in JSON format.
In this file we set the IP of the MongoDB service to \texttt{localhost}, which in our case is equivalent to set it to \texttt{192.168.1.10}, and may improve communications performance.

The \texttt{appstorage}, \texttt{inputstorage}, \texttt{outputstorage} and \texttt{rsync\_folder} are folder paths on the local machine where the storage service is being run.
In our case we are using only one processing node, so it must also contain enough storage capacity for saving data from Scife.

\paragraph{Minion configuration file}

A template of the minion configuration file can be found on the \textbf{extra} folder.
Using the information in \cref{tab:deployment}, we would create a \textbf{\texttt{/home/user/config/minion.cfg}} file with the following contents:
\textbf{Important note:} every minion has its own configuration file.

\begin{lstlisting}
{
	"listen": "tcp://0.0.0.0:4010",
	"db": "mongodb://localhost:4004/db",
	"url": "localhost",
	"username": "user",
	"authkeyfile": "/home/user/.ssh/id_rsa",
	"images": [
		{
			"internal_id": "81743971-6f56-4c9f-a557-2edf4516d185",
			"internal_sizes_compatible": ["2", "3", "5"],
			"name": "Cluster FS",
			"username": "user",
			"desc": "Description...",
			"workpath": "/home/user/workspace",
			"inputpath": "/home/user/inputdata",
			"outputpath": "/home/user/outputdata",
			"libpath": "/usr",
			"tmppath": "/tmp"
		}
	],
	"sizes": [
		{"name": "little", "desc": "Description", "cpus": 1, "ram": 512, "internal_id": "1"},
		{"name": "small", "desc": "Description", "cpus": 1, "ram": 2048, "internal_id": "2"},
		{"name": "normal", "desc": "Description", "cpus": 2, "ram": 4096, "internal_id": "3"},
		{"name": "big", "desc": "Description", "cpus": 4, "ram": 8192, "internal_id": "4"},
		{"name": "huge", "desc": "Description", "cpus": 8, "ram": 16384, "internal_id": "5"}
	]
}
\end{lstlisting}

Again, this minion will listen to any IP (\texttt{0.0.0.0}) on the \texttt{4010} port.
We also used the \texttt{localhost} address of MongoDB.

The \texttt{url} field is very important, as it contains the IP address of the target processing platform.
In our case, it is the same processing node so we use the \texttt{localhost} address.
The minion-platform connection is based on SSH, and we already configured it on \cref{sec:dependencies}.
The \texttt{username} correspond to the username used to perform the SSH connection on the target platform.
In our case is the \texttt{user} username.
We also need the path to the private key to connect to the target platform, which is specified in the \texttt{authkeyfile} field.

The \texttt{images} and \texttt{sizes} fields, are JSON arrays that determine the properties of each image and size provided by the target platform.
Each image models the environment of a computing instance, containing several paths: system libraries, processing, input files, output files, temporal\ldots
The \texttt{internal\_sizes\_compatible} is a special array on which we select which sizes (of the same minion) can be used with this image.
In the previous example, only the \texttt{small} (2), \texttt{normal} (3) and \texttt{huge} (5) sizes can be used with the available image.
Each size models processing capacity, being \texttt{cpus} the number of processing units and \texttt{ram} the available memory.

\paragraph{Rsync configuration file}

A template of the Rsync configuration file can be found on the \textbf{extra} folder.
Using the information in \cref{tab:deployment}, we create a \textbf{\texttt{/home/user/config/rsyncd.cfg}} file with the following contents:

\begin{lstlisting}
port = 4003
log file = /home/user/.forever/rsyncd.log
use chroot = no

&include /home/user/rsync_conn/
\end{lstlisting}

The format of this file is totally different from the other services, as it depends on the Rsync service, which is independent of the Scife project.
In this file we set: the \texttt{port} field to its corresponding Rsync port, the path for the log file and the path to the Rsync folder.
The path to the Rsync folder, \texttt{/home/user/rsync\_conn/} in our case, must match the path of the \texttt{rsync\_folder} field in the \texttt{storage.cfg} file.
The final ``\texttt{/}'' character of the last line is important so make sure to include it.

\subsubsection{Modifying the launch script}

The launch script is optional, as it is not a configuration file \textit{per se}.
An example of a launch script named \texttt{start.sh} can be found in Scife's root folder.
The objective of this file is to launch Scife's services using the configuration files we created.
The next paragraphs explain the contents of this file.

\paragraph{Preamble}

On the first lines we can find:
\begin{lstlisting}[numbers=left]
#!/bin/bash

#######################################################
#######################################################
#
# Example script for launching Scife and one minion
#
#######################################################
#######################################################

echo "THIS IS AN EXAMPLE SCRIPT! Please, do not run it without modifying it."
exit 1

# Load environment
. ~/.nvm/nvm.sh 2>/dev/null
. ~/.bashrc 2>/dev/null

#############################################"
# SCIFE FOLDER
#############################################"
SCIFE_PATH=/home/user/scife
\end{lstlisting}

The first line specifies that this file is a shell script (Bash).
If we try to run this script without modifying it, it will print the ``\texttt{THIS IS AN EXAMPLE SCRIPT! Please, do not run it without modifying it.}'' message to the screen and terminate.
This behavior is to enforce the user to configure it before using it.
Before continuing, comment out lines 11 and 12 by inserting ``\#'' characters at the beggining of the lines.

Then we have the ``\texttt{\# Load environment}'' block, which is necessary to load NodeJS using NVM.
Next, we have the \texttt{SCIFE\_PATH} variable, that must be changed to Scife's folder, so make sure that the path is correct.
In our example, the path should be ``\texttt{/home/user/scife}''.

\paragraph{Configuration files}

The next lines of the script contain:

\begin{lstlisting}
#############################################"
# CONFIGURATION FILES
#############################################"
CFG_OVERLORD="./extra/overlord.cfg"
CFG_STORAGE="./extra/storage.cfg"
CFG_MINION="./extra/minion.cfg"
CFG_RSYNC="./extra/rsyncd.cfg"
\end{lstlisting}

These four variables specify the location of the configuration files, so we need to change them to the paths of our example:

\begin{lstlisting}
CFG_OVERLORD="/home/user/config/overlord.cfg"
CFG_STORAGE="/home/user/config/storage.cfg"
CFG_MINION="/home/user/config/minion.cfg"
CFG_RSYNC="/home/user/config/rsyncd.cfg"
\end{lstlisting}

\paragraph{Service parameters}

On the next lines of the file we can find the blocks related to the parameters of each service:
\begin{lstlisting}[numbers=left]
#############################################"
# NODE PARAMETERS
#############################################"
NODE=v5.5.0
#############################################"
# SCIFE PARAMETERS
#############################################"
BRANCH=develop
#############################################"
# DATABASE PARAMETERS
#############################################"
DB_UID="database"
DB_BIN="mongod"
DB_PATH="/home/mongodb"
DB_BIND="--bind_ip localhost"
DB_PORT="--port 27017"
#DB_NUMA="`which numactl`"
#DB_NUMA_OPTS="--interleave all"
#############################################"
# GIT DAEMON PARAMETERS
#############################################"
GD_UID="gitdaemon"
GD_BIN="git daemon"
GD_OPTS="--reuseaddr --verbose"
GD_PATH="$HOME/appstorage"
GD_PORT="--port=27478"
#############################################"
# RSYNC DAEMON PARAMETERS
#############################################"
RS_UID="rsyncd"
RS_BIN=`which rsync`
RS_OPTS="--daemon --no-detach --config=$CFG_RSYNC"
#############################################"
# WEB SERVER PARAMETERS
#############################################"
WB_UID="web"
WB_BIN="server.js"
WB_OPTS="web.cfg"
WB_PATH="$HOME/multicloud-web/public-server"
#############################################"
\end{lstlisting}

On the \texttt{NODE} variable (ln. 4), we can change the version of NodeJS to use.
This is the version that the NVM will try to load.

You can ignore \texttt{BRANCH} variable (ln. 8) for now, as it handles the auto update feature (currently unsupported).

From line 9 to 18, we have the parameters of the database service (MongoDB).
The \texttt{DB\_UID} variable (ln. 12) is the name for tracking the database service using the \textbf{forever} module.
In general, each \texttt{XX\_UID} variable is used for that purpose, for each service.
The \texttt{DB\_PATH} variable (ln. 14) set the path on which the database files are stored, so we need to change it to ``\texttt{/home/user/mongodb}''.
The \texttt{DB\_BIND} variable (ln. 15) set the IPs on which MongoDB will listen for connections and the \texttt{DB\_PORT} variable (ln. 16) set the listening port.
Therefore, we can use the \texttt{localhost} IP for listening but we need to change the port to use the 4004 (see \cref{tab:deployment}).
The database block should look like this:
\begin{lstlisting}
#############################################"
# DATABASE PARAMETERS
#############################################"
DB_UID="database"
DB_BIN="mongod"
DB_PATH="/home/user/mongodb"
DB_BIND="--bind_ip localhost"
DB_PORT="--port 4004"
#DB_NUMA="`which numactl`"
#DB_NUMA_OPTS="--interleave all"
\end{lstlisting}

The variables on the block for configuring the Git daemon (ln. 19 to 26) are very similar to the ones for the database.
The \texttt{GD\_UID} variable (ln. 22) is the tracking name.
We need to change the \texttt{GD\_PATH} to the path where the applications are stored, ``\texttt{/home/user/appstorage}'' in our case.
We also need to change the port on the \texttt{GD\_PORT} variable to 4002:
\begin{lstlisting}
#############################################"
# GIT DAEMON PARAMETERS
#############################################"
GD_UID="gitdaemon"
GD_BIN="git daemon"
GD_OPTS="--reuseaddr --verbose"
GD_PATH="/home/user/appstorage"
GD_PORT="--port=4002"
\end{lstlisting}

Regarding the Rsync daemon block (ln. 27 to 32), no changes are needed.
In the case of the web server block (ln. 33 to 39), the \texttt{WB\_UID} sets the tracking name, the \texttt{WB\_OPTS} sets the configuration file that the web server will use (see the web server project).
Finally the \texttt{WB\_PATH} sets the path in which the webserver is installed.

\paragraph{The rest of the file}

In principle, the rest of this script is not needed to be modified.

\subsubsection{Running the launch script}

The last step consists in executing the modified launch script.
The script will run each of Scife's services, using the modified configurations files.
The services will be run using the \texttt{forever} module.
This NodeJS module ensures that the service will be restarted in case of a crash.
Additionally, \texttt{forever} will save the logs of each service in the ``\texttt{/home/user/.forever}'' path.

After running the script, and if everything went well, Scife framework should be listening on port 4000.

\end{document}
