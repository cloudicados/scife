#!/bin/sh

# Is uuidgen present?
command -v uuidgen > /dev/null 2>&1 && { uuidgen; exit 0; }

# Is Linux random/uuid present?
UUID="/proc/sys/kernel/random/uuid"
if [[ -f "$UUID" ]]; then
	cat $UUID
	exit 0
fi

# Is FreeBSD random/uuid present?
UUID="/compat/linux/proc/sys/kernel/random/uuid"
if [[ -f "$UUID" ]]; then
	cat $UUID
	exit 0
fi
